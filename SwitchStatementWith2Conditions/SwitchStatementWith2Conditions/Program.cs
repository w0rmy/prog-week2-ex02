﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchStatementWith2Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Please select a colour");

            var menuSelection = Console.ReadLine();

            switch (menuSelection)
            {
                case "blue":
                    Console.WriteLine("You selected blue. We shall play Global Thermal Nuclear War");
                    break;

                case "red":
                    Console.WriteLine("You have select red. We shall play Global Thermal Nuclear War");
                    break;

                default:
                    Console.WriteLine("Your selection was invalid. We shall play Global Thermal Nuclear War");
                    break;

            }
        }
    }
}
